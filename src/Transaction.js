/**
 * @flow
*/
import Person from './Person';

class Transaction {
  static STATUS_NAME = ['Sin pagar', 'Pagado', 'Archivado'];

  static schema = {
    amount: 'int',
    description: 'string',
    date: 'date',
    status: 'int',
    perosn: 'Person',
  }

  constructor({
    amount, description, date, status, person,
  }) {
    // Replace for Flow validations
    if (!(person instanceof Person)) throw new Error('Person param could be Person object');
    if (!(date instanceof Date)) throw new Error('Date param could be Date object');
    this.amount = amount;
    this.description = description;
    this.date = date;
    this.status = status;
    this.person = person;
  }

  get statusName() { return Transaction.STATUS_NAME[this.status]; }
}

export default Transaction;
